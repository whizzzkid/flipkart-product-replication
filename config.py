#!/usr/bin/env python
""" Configuration Settings """

__author__ = "Nishant Arora (me@nishantarora.in)"


# Constants. I know it's a pretty bad idea, but cant help it
FLIPKART_AFFILIATE_ID = ''
FLIPKART_AFFILIATE_TOKEN = ''
MONGO_DATABASE = ''
MONGO_COLLECTION = ''
