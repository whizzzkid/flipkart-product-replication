#!/usr/bin/env python
""" This script will help replicate Flipkart's product database using their
affiliate APIs"""

__author__ = "Nishant Arora (me@nishantarora.in)"


# Configuration Settings
import config


# Flipkart wants us to have following headers so we can access their APIs
API_HEADERS = {
    'Fk-Affiliate-Id': config.FLIPKART_AFFILIATE_ID,
    'Fk-Affiliate-Token': config.FLIPKART_AFFILIATE_TOKEN
}


# Initial seed url.
CATEGORY_FEED_URL = \
'https://affiliate-api.flipkart.net/affiliate/api/' + \
config.FLIPKART_AFFILIATE_ID + '.json'


def time_stamp():
    ''' Returns the current timestamp. '''
    import datetime, time
    return datetime.datetime.fromtimestamp(time.time()). \
    strftime('%Y-%m-%d %H:%M:%S')


def logger(msg):
    ''' Helps log messages with timestamps '''
    print time_stamp(), msg


def output_json(json_object):
    ''' Just used for debugging, pretty prints json '''
    import json
    print json.dumps(json_object, indent=2, sort_keys=True)


def fetch_json_from_url(url):
    ''' Fetches JSON response from the Flipkart's API endpoint. '''
    import requests
    fetch = requests.get(url, headers=API_HEADERS)
    if fetch.status_code != 200:
        logger('Fetch error, code != 200')
    return fetch.json()


def fetch_category_feed():
    ''' Fetches feed for all categories. '''
    category_feed_response = fetch_json_from_url(CATEGORY_FEED_URL)
    return category_feed_response['apiGroups']['affiliate']['apiListings']


def process_category(category_url, name, run):
    ''' Processes categories found from the URLs found in the feed. '''
    logger('Fetch -> ' + name + '[Category: ' + str(CATEGORY_COUNT) \
    + ', Run: ' + str(run) + ']')
    category_content=fetch_json_from_url(category_url)
    push_to_database(category_content['productInfoList'], name)
    logger('Fetch Complete -> ' + name)
    if category_content['nextUrl']:
        #category_run += 1
        logger(category_content['nextUrl'])
        logger('Next Page Found -> ' + name)
        process_category(category_content['nextUrl'], name, run+1)


def push_to_database(json_objects, name):
    ''' Saves the JSON objects to the mongo db. '''
    logger('Save -> ' + name)
    from pymongo import MongoClient
    mongo_db_client = MongoClient()
    mongo_db = mongo_db_client[config.MONGO_DATABASE]
    mongo_collection = mongo_db[config.MONGO_COLLECTION]
    logger('Inserting -> ' + str(len(json_objects)) + ' objects')
    for product_object in json_objects:
        mongo_collection.insert(product_object)
    logger('Save Complete -> ' + name)


# Init
logger('Replications started')
logger('Fetch -> category feed url')
CATEGORY_FEED_CONTENT = fetch_category_feed()
CATEGORY_COUNT = 0
for category in CATEGORY_FEED_CONTENT:
    CATEGORY_COUNT += 1
    logger('Processing Category: ' + category)
    process_category( \
    CATEGORY_FEED_CONTENT[category] \
    ['availableVariants']['v0.1.0']['get'],\
    category, 1)
logger('Replication Successful')
